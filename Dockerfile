# Use Debian slim as the base image
FROM debian:bookworm-slim
# Peter van Eijk - october 2023

# Install necessary dependencies and tools
RUN apt-get update && apt-get install -y --no-install-recommends \
    ca-certificates \
    apt-transport-https \
    lsb-release \
    gnupg2 \
    python3 \
    python3-pip \
    pipx \
    git \
    jq \
    less \
    vim \
    sudo \
    wget curl \
    nmap traceroute iputils-ping dnsutils\
    ssh \
    && apt update \
    && apt-get clean

RUN pipx ensurepath

RUN apt-get install -y --no-install-recommends \
    awscli \
    kubernetes-client

RUN curl -sL https://aka.ms/InstallAzureCLIDeb | bash

RUN curl -fsSL https://get.docker.com -o get-docker.sh
RUN sh get-docker.sh

RUN curl https://apt.releases.hashicorp.com/gpg | gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
RUN echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" |  tee /etc/apt/sources.list.d/hashicorp.list
RUN apt update && apt install terraform

# Install Google Cloud SDK
RUN curl -fsSL https://dl.google.com/dl/cloudsdk/channels/rapid/google-cloud-sdk.tar.gz | tar xz -C / && \
    /google-cloud-sdk/install.sh -q --rc-path /etc/bash.bashrc

RUN pipx install prowler

RUN useradd -ms /bin/bash cliuser && chown cliuser /home/cliuser
RUN usermod -aG sudo cliuser
RUN echo "alias k=kubectl ; echo Readme at https://gitlab.com/pveijk/cloudwenger/" >>/etc/bash.bashrc
RUN echo "cliuser:cliuser" | chpasswd
USER cliuser
WORKDIR /home/cliuser

# Set the default shell for the container to /bin/sh
CMD ["/bin/bash"]

# docker build -t cloudwenger .
# docker run -it --rm -v `pwd`:/home/cliuser cloudwenger 
# docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock cloudwenger # non root? 

