# CloudWenger: Image to run cloud command lines

## Description

A Docker image to provide many cloud provider command line interfaces.
Useful in conjunction with the CCSK (Certificate of Cloud Security Knowledge) labs, and my own [cloud workshops](https://workshop.clubcloudcomputing.com).

Consider this the 'Swiss Army Knife' of cloud command lines. The name CloudWenger refers to the company manufacturing the original Swiss Army knife.

## Cloud command lines

The idea is to run a container which can open as a shell giving you access to the following command line tools.

The following table has links to more documentation.

| Service | Command |
| --- | --- |
| [Amazon Web Services](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html) | ```aws``` |
| [Microsoft Azure](https://learn.microsoft.com/nl-nl/cli/azure/get-started-with-azure-cli) | ```az```|
| [Google Cloud Platform](https://cloud.google.com/sdk/gcloud) | ```gcloud```|
| [Kubernetes](https://kubernetes.io/docs/reference/kubectl/) | ```kubectl```|
| [Docker](https://docs.docker.com/engine/reference/commandline/cli/) | ```docker```|
| [Terraform](https://developer.hashicorp.com/terraform/cli/commands) | ```terraform```|
| [Prowler](https://docs.prowler.cloud/en/latest/) | ```prowler```|

Additional commands: ```sudo```, ```git```, ```nmap```, ```jq``` , ```dnsutils```, and some more utilities.

## Usage

Make sure you have Docker installed. Run the container to start the shell.

``` shell
docker run -it --rm registry.gitlab.com/pveijk/cloudwenger 
```

Optionally mount the current local (host) directory to persist credentials and other files. MacOS and Windows version.

``` shell mac
docker run -it --rm -v `pwd`:/home/cliuser registry.gitlab.com/pveijk/cloudwenger 
```

``` windows command line
docker run -it --rm -v %cd%:/home/cliuser registry.gitlab.com/pveijk/cloudwenger 
```

And/or mount the docker socket to view docker on the host (requires root priviliges).

``` shell
docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock registry.gitlab.com/pveijk/cloudwenger
```

You will need to ```sudo``` to get docker commands to work. The default user is ```cliuser``` with password ```cliuser```.

## Examples

Once you have a prompt in the container, you should be able to run the following examples:

```shell
aws --version
aws configure
aws s3 ls
sudo docker images
```

## Tips

- AWS CLI Profile management: [blog](https://www.linkedin.com/pulse/aws-cli-multiple-profiles-bachar-hakam/)
- Secrets management with [Doppler](https://www.doppler.com)

## Support

Please use the gitlab issue tracker. Note that CloudWenger does not in every case follow the most up to date versions of these tools. The Dockerfile is your guide.

## Roadmap

Let me know your use case.

## Contributing

Feel free to raise issues and pull requests.

## Authors and acknowledgment

Thanks to all the providers of command line tools.

## License

Thinking about the license.

## Status

Wishlist: more ways of secrets injection. You might want to avoid having them on your host. ENV is not the answer. Doppler might be.
